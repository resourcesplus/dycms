<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Checkadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // 暂时屏蔽
        if (!Auth::guard('admin')->check()) {

            return redirect(route(env('ADMIN_PREFIX', 'admin').'.login'));
        }else{
            return $next($request);
        }

    }
}
